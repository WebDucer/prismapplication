﻿using System.Windows;
using de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces;
using de.webducer.dotnet.PrismApplication.Shell.Logging;
using de.webducer.dotnet.PrismApplication.Shell.Views;
using Microsoft.Practices.Prism.Logging;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.UnityExtensions;
using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;

namespace de.webducer.dotnet.PrismApplication.Shell.BusinessLogic {
   public class PrismBootstrapper : UnityBootstrapper {
      protected override DependencyObject CreateShell() {
         Logger.Log(@"Entry: CreateChell()", Category.Debug, Priority.Low);

         return ServiceLocator.Current.GetInstance<MainWindow>();
      }

      protected override void InitializeShell() {
         Logger.Log(@"Entry: InitializeShell()", Category.Debug, Priority.Low);

         base.InitializeShell();

         Application.Current.MainWindow = (Window) Shell;
         Application.Current.MainWindow.Show();
      }

      protected override ILoggerFacade CreateLogger() {
         return new NLogLoggerFacade();
      }

      protected override void ConfigureServiceLocator() {
         Logger.Log(@"Entry: ConfigureServiceLocator()", Category.Debug, Priority.Low);

         base.ConfigureServiceLocator();

         Container.RegisterType<IClassLoggerService, NLogClassLoggerService>(new ContainerControlledLifetimeManager());
         Container.RegisterType<IDiContainer, UnityDiContainerAdapter>(new ContainerControlledLifetimeManager());
      }

      protected override IModuleCatalog CreateModuleCatalog() {
         Logger.Log(@"Entry: CreateModuleCatalog()", Category.Debug, Priority.Low);

         return new DirectoryModuleCatalog {
            ModulePath = @".\Modules"
         };
      }

      protected override void ConfigureModuleCatalog() {
         base.ConfigureModuleCatalog();

         var statusbarModulType = typeof (StatusbarModul.StatusbarModul);
         ModuleCatalog.AddModule(new ModuleInfo {
            ModuleName = statusbarModulType.Name,
            ModuleType = statusbarModulType.AssemblyQualifiedName,
            InitializationMode = InitializationMode.WhenAvailable
         });
      }
   }
}