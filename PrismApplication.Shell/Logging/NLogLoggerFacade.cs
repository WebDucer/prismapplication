﻿using Microsoft.Practices.Prism.Logging;
using NLog;

namespace de.webducer.dotnet.PrismApplication.Shell.Logging {
   public class NLogLoggerFacade : ILoggerFacade {
      private const string _PRISM_DEFAULT_LOGGER = "PrismLogger";
      private readonly Logger _log = LogManager.GetLogger(_PRISM_DEFAULT_LOGGER);

      #region ILoggerFacade Member

      public void Log(string message, Category category, Priority priority) {
         switch (category) {
            case Category.Debug:
               _log.Debug(message);
               break;

            case Category.Info:
               _log.Info(message);
               break;

            case Category.Warn:
               _log.Warn(message);
               break;

            case Category.Exception:
               _log.Error(message);
               break;

            default:
               _log.Debug(message);
               break;
         }
      }

      #endregion
   }
}