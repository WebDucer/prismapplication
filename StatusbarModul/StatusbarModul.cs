﻿using de.webducer.dotnet.PrismApplication.Infrastructure.Base;
using de.webducer.dotnet.PrismApplication.Infrastructure.Constants;
using de.webducer.dotnet.PrismApplication.StatusbarModul.Views;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;

namespace de.webducer.dotnet.PrismApplication.StatusbarModul {
   [Module(ModuleName = "StatusbarModul", OnDemand = true)]
   public class StatusbarModul : BaseModule {
      public override void Initialize() {
         RegionManager.RegisterViewWithRegion(RegionNames.STATUSBAR_REGION, typeof (StatusbarView));
      }
   }
}