﻿using System.Reflection;
using System.Windows.Controls;
using de.webducer.dotnet.PrismApplication.Infrastructure.Base;
using de.webducer.dotnet.PrismApplication.Infrastructure.SharedEvents;
using de.webducer.dotnet.PrismApplication.Infrastructure.Shared_Objects;

namespace de.webducer.dotnet.PrismApplication.StatusbarModul.ViewModels {
   public class StatusbarViewModel : BaseViewModel {
      public StatusbarViewModel() {
         if (IsInDesignModeStatic) {
            LoadDesignTimeData();
         }
         else {
            Container.GetEvent<StatusbarStateChangedEvent>().Subscribe(s => StatusMessage = s);
            Version = Assembly.GetExecutingAssembly().GetName().Version.ToString();
            Container.GetEvent<CustomObjectChangedEvent>().Subscribe(s => CustomObject = s);
         }
      }

      /// <summary>
      ///    The <see cref="StatusMessage" /> property's name.
      /// </summary>
      public const string StatusMessagePropertyName = "StatusMessage";

      private string _statusMessage = null;

      /// <summary>
      ///    Sets and gets the StatusMessage property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public string StatusMessage {
         get {
            return _statusMessage;
         }
         private set {
            Set(() => StatusMessage, ref _statusMessage, value);
         }
      }

      /// <summary>
      ///    The <see cref="Version" /> property's name.
      /// </summary>
      public const string VersionPropertyName = "Version";

      private string _version = null;

      /// <summary>
      ///    Sets and gets the Version property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public string Version {
         get {
            return _version;
         }
         private set {
            Set(() => Version, ref _version, value);
         }
      }

      private void LoadDesignTimeData() {
         StatusMessage = @"[Design] Message";
         Version = @"1.0.1.305";
      }

      /// <summary>
      /// The <see cref="CustomObject" /> property's name.
      /// </summary>
      public const string CustomObjectPropertyName = "CustomObject";

      private MySharedObject _customObject = null;

      /// <summary>
      /// Sets and gets the CustomObject property.
      /// Changes to that property's value raise the PropertyChanged event. 
      /// </summary>
      public MySharedObject CustomObject {
         get {
            return _customObject;
         }
         set {
            Set(() => CustomObject, ref _customObject, value);
         }
      }
   }
}