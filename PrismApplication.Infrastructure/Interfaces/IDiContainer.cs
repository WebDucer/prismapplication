﻿using System;
using Microsoft.Practices.Prism.PubSubEvents;
using Microsoft.Practices.ServiceLocation;

namespace de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces {
   public interface IDiContainer : IServiceLocator, IServiceProvider {
      IDiContainer RegisterInstance(Type t, string name, object instance, LifeType lifeType = LifeType.Default);

      IDiContainer RegisterInstance(Type t, object instance, LifeType lifeType = LifeType.Default);

      IDiContainer RegisterInstance<TInterface>(string name, TInterface instance, LifeType lifeType = LifeType.Default);

      IDiContainer RegisterInstance<TInterface>(TInterface instance, LifeType lifeType = LifeType.Default);

      IDiContainer RegisterType(Type from, Type to, string name, LifeType lifeType = LifeType.Default);

      IDiContainer RegisterType(Type from, Type to, LifeType lifeType = LifeType.Default);

      IDiContainer RegisterType<T>(string name, LifeType lifeType = LifeType.Default);

      IDiContainer RegisterType<T>(LifeType lifeType = LifeType.Default);

      IDiContainer RegisterType<TFrom, TTo>(string name, LifeType lifeType = LifeType.Default) where TTo : TFrom;

      IDiContainer RegisterType<TFrom, TTo>(LifeType lifeType = LifeType.Default) where TTo : TFrom;

      T GetEvent<T>() where T : EventBase, new();
   }

   public enum LifeType : byte {
      Default = 0,
      Singleton = 1,
      PerThread = 2
   }
}