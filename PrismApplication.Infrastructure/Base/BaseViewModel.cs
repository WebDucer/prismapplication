﻿using System;
using de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces;
using GalaSoft.MvvmLight;
using Microsoft.Practices.ServiceLocation;

namespace de.webducer.dotnet.PrismApplication.Infrastructure.Base {
   public abstract class BaseViewModel : ViewModelBase, IDisposable {
      protected readonly IClassLogger Logger;
      protected readonly IDiContainer Container;

      protected BaseViewModel() {
         Container = ServiceLocator.Current.GetInstance<IDiContainer>();
         Logger = Container.GetInstance<IClassLoggerService>().GetClassLogger(GetType());
      }

      public void Dispose() {
         Dispose(true);
         GC.SuppressFinalize(this);
      }

      protected virtual void Dispose(bool disposing) {}
   }
}