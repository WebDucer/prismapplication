﻿using de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace de.webducer.dotnet.PrismApplication.Infrastructure.Base {
   public abstract class BaseModule : IModule {
      protected readonly IDiContainer Container;
      protected readonly IClassLogger Logger;
      protected readonly IRegionManager RegionManager;

      protected BaseModule() {
         Container = ServiceLocator.Current.GetInstance<IDiContainer>();
         Logger = Container.GetInstance<IClassLoggerService>().GetClassLogger(GetType());
         RegionManager = Container.GetInstance<IRegionManager>();
      }

      public abstract void Initialize();
   }
}