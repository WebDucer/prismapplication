﻿using Microsoft.Practices.Prism.PubSubEvents;

namespace de.webducer.dotnet.PrismApplication.Infrastructure.SharedEvents {
   public class StatusbarStateChangedEvent : PubSubEvent<string> {}
}