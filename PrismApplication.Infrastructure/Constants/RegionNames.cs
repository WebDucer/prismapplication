﻿namespace de.webducer.dotnet.PrismApplication.Infrastructure.Constants {
   public static class RegionNames {
      public const string MENU_REGION = "Region.MainMenu";

      public const string TOOLBAR_REGION = "Region.MainToolbar";

      public const string CONTENT_REGION = "Region.MainContent";

      public const string STATUSBAR_REGION = "Region.MainStatusbar";
   }
}