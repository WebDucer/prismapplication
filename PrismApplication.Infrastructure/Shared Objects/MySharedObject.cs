﻿namespace de.webducer.dotnet.PrismApplication.Infrastructure.Shared_Objects
{
   public class MySharedObject
   {
      public MySharedObject(string text)
      {
         FixedText = @"I recieved: ";
         CustomText = text;
      }

      public string FixedText { get; set; }
      public string CustomText { get; set; }

      public string FullText
      {
         get { return FixedText + CustomText; }
      }
   }
}