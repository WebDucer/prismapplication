﻿using de.webducer.dotnet.PrismApplication.ClickableButtons.Views;
using de.webducer.dotnet.PrismApplication.Infrastructure.Base;
using de.webducer.dotnet.PrismApplication.Infrastructure.Constants;
using Microsoft.Practices.Prism.Modularity;
using Microsoft.Practices.Prism.Regions;

namespace de.webducer.dotnet.PrismApplication.ClickableButtons
{
   [Module(ModuleName = "ClickableButtons", OnDemand = false)]
   public class ClickableButtons : BaseModule
   {
      public override void Initialize()
      {
         Logger.Debug(@"Enty: {0}", @"Initialize");

         //direkt in Toolbar anzeigen
         RegionManager.RegisterViewWithRegion(RegionNames.TOOLBAR_REGION, typeof (MenuView));

         //für späteres anzeigen vormerken
         Container.RegisterType<object, ContentView>(typeof (ContentView).FullName);

         Logger.Debug(@"Exit: {0}", @"Initialize");
      }
   }
}