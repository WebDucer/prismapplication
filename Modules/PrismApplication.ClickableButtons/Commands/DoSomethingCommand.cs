﻿using System;
using System.Windows.Input;
using de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces;
using de.webducer.dotnet.PrismApplication.Infrastructure.SharedEvents;
using de.webducer.dotnet.PrismApplication.Infrastructure.Shared_Objects;
using Microsoft.Practices.ServiceLocation;

namespace de.webducer.dotnet.PrismApplication.ClickableButtons.Commands
{
   public class DoSomethingCommand : ICommand
   {
      public bool CanExecute(object parameter)
      {
         return true;
      }

      public void Execute(object parameter)
      {
         var container = ServiceLocator.Current.GetInstance<IDiContainer>();
         var logger = container.GetInstance<IClassLoggerService>().GetClassLogger(GetType());
         logger.Info("Button wurde geklickt");
         container.GetEvent<CustomObjectChangedEvent>().Publish(new MySharedObject(@"Hier steht ein toller Text!"));
      }

      public event EventHandler CanExecuteChanged;
   }
}