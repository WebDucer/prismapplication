﻿using System.Windows.Input;
using de.webducer.dotnet.PrismApplication.ClickableButtons.Commands;
using de.webducer.dotnet.PrismApplication.Infrastructure.Base;
using de.webducer.dotnet.PrismApplication.Infrastructure.Interfaces;
using de.webducer.dotnet.PrismApplication.Infrastructure.Shared_Objects;
using Microsoft.Practices.Prism.Regions;
using Microsoft.Practices.ServiceLocation;

namespace de.webducer.dotnet.PrismApplication.ClickableButtons.ViewModels
{
   public class ContentViewModel : BaseViewModel, INavigationAware
   {
      #region Constuctor

      public ContentViewModel()
      {
         
      }
      #endregion

      #region AnzeigeText

      /// <summary>
      ///    The <see cref="AnzeigeText" /> property's name.
      /// </summary>
      public const string AnzeigeTextPropertyName = "AnzeigeText";

      private string _anzeigeText = "";

      /// <summary>
      ///    Sets and gets the AnzeigeText property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public string AnzeigeText
      {
         get { return _anzeigeText; }
         set { Set(() => AnzeigeText, ref _anzeigeText, value); }
      }

      #endregion

      #region DoSomethingCommand

      /// <summary>
      ///    The <see cref="DoSomethingCommand" /> property's name.
      /// </summary>
      public const string DoSomethingCommandPropertyName = "DoSomethingCommand";

      private ICommand _doSomethingCommand = new DoSomethingCommand();

      /// <summary>
      ///    Sets and gets the DoSomethingCommand property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public ICommand DoSomethingCommand
      {
         get { return _doSomethingCommand; }
         set { Set(() => DoSomethingCommand, ref _doSomethingCommand, value); }
      }

      #endregion

      #region Implementation INavigationAware

      public void OnNavigatedTo(NavigationContext navigationContext)
      {
         var myObject = navigationContext.Parameters["Key1"] as MySharedObject;
         if (myObject != null)
         {
            var container = ServiceLocator.Current.GetInstance<IDiContainer>();
            var logger = container.GetInstance<IClassLoggerService>().GetClassLogger(GetType());
            logger.Info(myObject.FullText);
            AnzeigeText = myObject.FullText;
         }
      }

      public bool IsNavigationTarget(NavigationContext navigationContext)
      {
         return true;
      }

      public void OnNavigatedFrom(NavigationContext navigationContext)
      {
      }

      #endregion
   }
}