﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using de.webducer.dotnet.PrismApplication.ClickableButtons.Views;
using de.webducer.dotnet.PrismApplication.Infrastructure.Base;
using de.webducer.dotnet.PrismApplication.Infrastructure.Constants;
using de.webducer.dotnet.PrismApplication.Infrastructure.SharedEvents;
using Microsoft.Practices.Prism.Commands;
using Microsoft.Practices.Prism.Regions;

namespace de.webducer.dotnet.PrismApplication.ClickableButtons.ViewModels {
   public class MenuViewModel : BaseViewModel {
      public MenuViewModel()
      {
         ShowContentCommand = new DelegateCommand(OnShowContent);
      }

      /// <summary>
      /// The <see cref="ShowContentCommand" /> property's name.
      /// </summary>
      public const string ShowContentCommandPropertyName = "ShowContentCommand";

      private ICommand _showContentCommand = null;

      /// <summary>
      /// Sets and gets the ShowContentCommand property.
      /// Changes to that property's value raise the PropertyChanged event. 
      /// </summary>
      public ICommand ShowContentCommand {
         get {
            return _showContentCommand;
         }
         set {
            Set(() => ShowContentCommand, ref _showContentCommand, value);
         }
      }

      private void OnShowContent() {
         var regionManager = Container.GetInstance<IRegionManager>();
         regionManager.RequestNavigate(RegionNames.CONTENT_REGION,
            new Uri(typeof (ContentView).FullName, UriKind.Relative),
            r => Container.GetEvent<StatusbarStateChangedEvent>().Publish(@"View from 'ClickableButtons' loaded!"));
      }
   }
}
