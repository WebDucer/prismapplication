﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Input;
using de.webducer.dotnet.PrismApplication.Infrastructure.Base;
using de.webducer.dotnet.PrismApplication.NLogOutputModule.Logging;
using Microsoft.Practices.Prism.Commands;
using NLog;
using NLog.Config;

namespace de.webducer.dotnet.PrismApplication.NLogOutputModule.ViewModels {
   public class NLogOutputViewModel : BaseViewModel {
      private readonly SimpleLambdaTarget _target;
      private LoggingRule _rule;

      public NLogOutputViewModel() {
         if (IsInDesignModeStatic) { return; }

         var config = LogManager.Configuration;
         _target = new SimpleLambdaTarget(s => NewLogMessage = s);
         config.AddTarget("NLogOutputModule", _target);
         _rule = new LoggingRule(string.Format("{0}*", FilterText ?? string.Empty), SelectedLogLevel, _target);
         config.LoggingRules.Add(_rule);

         LogManager.Configuration = config;

         LogCommand = new DelegateCommand(Log);
         ClearLogCommand = new DelegateCommand(() => {
            FullLog.Clear();
            RaisePropertyChanged(() => FullLog);
         });
      }

      #region Properties

      /// <summary>
      ///    The <see cref="LogLevels" /> property's name.
      /// </summary>
      public const string LogLevelsPropertyName = "LogLevels";

      private static readonly IEnumerable<LogLevel> _logLevels = new List<LogLevel> {
         LogLevel.Debug,
         LogLevel.Error,
         LogLevel.Fatal,
         LogLevel.Info,
         LogLevel.Off,
         LogLevel.Trace,
         LogLevel.Warn
      }.OrderBy(o => o.Ordinal).ToArray();

      /// <summary>
      ///    Sets and gets the LogLevels property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public IEnumerable<LogLevel> LogLevels {
         get {
            return _logLevels;
         }
      }

      /// <summary>
      ///    The <see cref="SelectedLogLevel" /> property's name.
      /// </summary>
      public const string SelectedLogLevelPropertyName = "SelectedLogLevel";

      private LogLevel _selectedLogLevel = LogLevel.Trace;

      /// <summary>
      ///    Sets and gets the SelectedLogLevel property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public LogLevel SelectedLogLevel {
         get {
            return _selectedLogLevel;
         }
         set {
            if (Set(() => SelectedLogLevel, ref _selectedLogLevel, value)) {
               ReconfigLogging();
            }
         }
      }

      /// <summary>
      ///    The <see cref="FilterText" /> property's name.
      /// </summary>
      public const string FilterTextPropertyName = "FilterText";

      private string _filterText = null;

      /// <summary>
      ///    Sets and gets the FilterText property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public string FilterText {
         get {
            return _filterText;
         }
         set {
            if (Set(() => FilterText, ref _filterText, value)) {
               ReconfigLogging();
            }
         }
      }

      /// <summary>
      ///    The <see cref="FullLog" /> property's name.
      /// </summary>
      public const string FullLogPropertyName = "FullLog";

      private readonly StringBuilder _fullLog = new StringBuilder();

      /// <summary>
      ///    Sets and gets the FullLog property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public StringBuilder FullLog {
         get {
            return _fullLog;
         }
      }

      /// <summary>
      ///    The <see cref="NewLogMessage" /> property's name.
      /// </summary>
      public const string NewLogMessagePropertyName = "NewLogMessage";

      private string _newLogMessage = null;

      /// <summary>
      ///    Sets and gets the NewLogMessage property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public string NewLogMessage {
         get {
            return _newLogMessage;
         }
         private set {
            if (Set(() => NewLogMessage, ref _newLogMessage, value)) {
               FullLog.AppendLine(value);
               RaisePropertyChanged(() => FullLog);
            }
         }
      }

      /// <summary>
      ///    The <see cref="LogCommand" /> property's name.
      /// </summary>
      public const string LogCommandPropertyName = "LogCommand";

      private ICommand _logCommand = null;

      /// <summary>
      ///    Sets and gets the LogCommand property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public ICommand LogCommand {
         get {
            return _logCommand;
         }
         set {
            Set(() => LogCommand, ref _logCommand, value);
         }
      }

      /// <summary>
      ///    The <see cref="ClearLogCommand" /> property's name.
      /// </summary>
      public const string ClearLogCommandPropertyName = "ClearLogCommand";

      private ICommand _clearLogCommand = null;

      /// <summary>
      ///    Sets and gets the ClearLogCommand property.
      ///    Changes to that property's value raise the PropertyChanged event.
      /// </summary>
      public ICommand ClearLogCommand {
         get {
            return _clearLogCommand;
         }
         set {
            Set(() => ClearLogCommand, ref _clearLogCommand, value);
         }
      }

      private void Log() {
         var logger = LogManager.GetCurrentClassLogger();
         logger.Debug(Guid.NewGuid());
         logger.Error(Guid.NewGuid());
         logger.Fatal(Guid.NewGuid());
         logger.Info(Guid.NewGuid());
         logger.Trace(Guid.NewGuid());
         logger.Warn(Guid.NewGuid());
      }

      private void ReconfigLogging() {
         var config = LogManager.Configuration;
         config.LoggingRules.Remove(_rule);
         _rule = new LoggingRule(string.Format("{0}*", FilterText ?? string.Empty), SelectedLogLevel, _target);
         config.LoggingRules.Add(_rule);

         LogManager.Configuration = config;
      }

      protected override void Dispose(bool disposing) {
         base.Dispose(disposing);

         if (disposing && _target != null) {
            _target.Dispose();
         }
      }

      #endregion
   }
}