﻿using System.Windows.Controls;

namespace de.webducer.dotnet.PrismApplication.MenuStartableModule.Views
{
   /// <summary>
   ///    Interaktionslogik für ContentView.xaml
   /// </summary>
   public partial class ContentView : UserControl
   {
      public ContentView()
      {
         InitializeComponent();
      }
   }
}